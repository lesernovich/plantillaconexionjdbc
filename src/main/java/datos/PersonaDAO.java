package datos;

import static datos.Conexion.*;
import domain.Persona;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick
 */
public class PersonaDAO {

    private static final String SELECT = "SELECT id_persona, nombre, apellido, telefono FROM test.persona";
    
    public List<Persona> seleccionar() {
        Connection conexion = null; //se va a conectar a la base de datos
        PreparedStatement baseDatos = null; //va a enviar una instruccion sql a la base de datos conectada
        ResultSet resultado = null; //aqui va a guardar la informacion
        Persona persona = null;
        List<Persona> personas = new ArrayList<>();

        try {
            conexion = getConexion();
            baseDatos = conexion.prepareStatement(SELECT);
            resultado = baseDatos.executeQuery();
            while (resultado.next()) {
                persona = new Persona(resultado.getInt("id_persona"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getString("telefono"));
                personas.add(persona);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                close(resultado);
                close(baseDatos);
                close(conexion);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return personas;
    }
    
    private static final String INSERT = "INSERT INTO test.persona(nombre, apellido, telefono)VALUES (?, ?, ?)";
    public void insertar(Persona persona){
    Connection conexion = null;
    PreparedStatement instruccion = null;
    
        try {
            conexion= getConexion();
            instruccion = conexion.prepareStatement(INSERT);
            instruccion.setString(1, persona.getNombre());
            instruccion.setString(2, persona.getApellido());
            instruccion.setString(3, persona.getTelefono());
            instruccion.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
        try {
            close(instruccion);
            close(conexion);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        }
    }
    
//    private static final String UPDATE_NOMBRE = "UPDATE test.persona SET nombre=? WHERE id_persona=?;";
//    private static final String UPDATE_APELLIDO = "UPDATE test.persona SET apellido=? WHERE id_persona=?;" ;
//    private static final String UPDATE_TELEFONO = "UPDATE test.persona SET telefono=? WHERE id_persona=?;";
    private static final String UPDATE_ALL = "UPDATE test.persona SET nombre=?, apellido=?, telefono=? WHERE id_persona=?;";
    public void editarTodo(Persona persona){
    Connection conexion = null;
    PreparedStatement instruccion = null;
    
        try {
            conexion= getConexion();
            instruccion = conexion.prepareStatement(UPDATE_ALL);
            
            instruccion.setInt(4, persona.getIdPersona());
            instruccion.setString(1, persona.getNombre());
            instruccion.setString(2, persona.getApellido());
            instruccion.setString(3, persona.getTelefono());
            instruccion.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
        try {
            close(instruccion);
            close(conexion);
        } catch (SQLException ex) {
           ex.printStackTrace(System.out);
        }
        }
    }
    
    private static final String DELETE = "DELETE FROM test.persona WHERE id_persona=?";
    
    public void eliminar(Persona persona){
    Connection conexion = null;
    PreparedStatement instruccion = null;
    
        try {
            conexion = getConexion();
            instruccion = conexion.prepareStatement(DELETE);
            instruccion.setInt(1, persona.getIdPersona());
            instruccion.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
        try {
            close(instruccion);
            close(conexion);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        }
    }
    
}
